
$(document).ready(function() {
    var base = 'https://192.168.75.31/users'
    // ============= TEST LOGIN ============= //
    $('#test-login').click(function(){    
        $.ajax({
            url: base + '/login',
            type: 'POST',
            data: {
                login : $('#login').val(),
                password : $('#password').val()
            },
            dataType: 'json',
            success: function (data,status,request) {
                $("#login-message").toggleClass("text-success", true);
                $("#login-message").toggleClass("text-danger", false);
                $("#login-message").text("Login test passed!");
                let token = request.getResponseHeader("Authentication");
                localStorage.setItem("login", $('#login').val());
                sessionStorage.setItem("token", token);
                $("#token").text(token);
            },
            error: function(jqXHR, exception){
                $("#login-message").toggleClass("text-success", false);
                $("#login-message").toggleClass("text-danger", true);
                $("#login-message").text(
                    "Login test did not passed! Status : " + jqXHR.status +
                    " Error  : " + exception); 
            }
        });

    });

    // ============= TEST LOGOUT ============= //
    $('#test-logout').click(function(){
        $.ajax({
            url: base + '/logout',
            // url: 'http://192.168.75.31:8080/logout',
            type: 'DELETE',
            data: {
                login : $('#login').val()
            },
            dataType: 'json',
            success: function (data,status, request) {
                $("#logout-message").toggleClass("text-success", true);
                $("#logout-message").toggleClass("text-danger", false);
                $("#logout-message").text("Logout test passed!");
            },
            error: function(jqXHR, exception){
                $("#logout-message").toggleClass("text-success", false);
                $("#logout-message").toggleClass("text-danger", true);
                $("#logout-message").text(
                    "Logout test did not passed! Status : " + jqXHR.status + " Error  : " + exception);
            }
        });

    });

    $('#test-authenticate').click(function(){
        $.ajax({
            url: base + '/authenticate',
            // url: 'http://192.168.75.31:8080/authenticate',
            type: 'GET',
            data: {
                token : $('#token').text(),
                origin : $('#origin').val()
            },
            dataType: 'json',
            success: function (data, status) {
                $("#authenticate-message").toggleClass("text-success", true);
                $("#authenticate-message").toggleClass("text-danger", false);
                $("#authenticate-message").text("Authentification test passed!");
                console.log(sessionStorage.getItem("token"))
                
            },
            error: function(jqXHR, exception){
                $("#authenticate-message").toggleClass("text-success", false);
                $("#authenticate-message").toggleClass("text-danger", true);
                $("#authenticate-message").text(
                    "Authentification test did not passed! Status : " + jqXHR.status + " Error  : " + exception);
            }       
        });

    });

    $('#test-user').click(function(){    
        $.ajax({
            url: base + "/users/"+$('#login').val(),
            // url: "http://192.168.75.31:8080/users/"+$('#login').val(),
            type: 'GET',
            dataType: 'json',
            success: function (data, status) {
                $("#user-message").toggleClass("text-success", true);
                $("#user-message").toggleClass("text-danger", false);
                $("#user-message").text("User test passed!");
                
                $("#login-success").text("Login : " + data.login);
                $("#connected-success").text("Connected : " + data.connected);                
            },
            error: function(jqXHR, exception){
                $("#user-message").toggleClass("text-success", false);
                $("#user-message").toggleClass("text-danger", true);
                $("#user-message").text(
                    "User test did not passed! Status : " + jqXHR.status +" Error  : " + exception);
            }
        });
    });

}); 