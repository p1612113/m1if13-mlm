# Ameliorations apres la demo
 - button logout
 - demande d'autorisation pour la geolocalisation des le login (ceci permet que l'affichage d'un formulaire soit possible en cas de _permission denied_)

# TP1 & TP2

Par defaut il existe un admin avec mot de passe admin.
Liens utiles:

 - lien vers le [yaml](users/users-api.yml) fichier des users

 - lien vers le [json](users/users-api.json) fichier des users (descriptif de l'api users en format JSON)

 - lien vers [swagger](http://192.168.75.31:8080/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config) GUI

 - lien vers [client simple](https://192.168.75.31/public)

# TP3

 - Interface administrateur: Pour creer un jeu il suffit de selectioner un centre et un radius, une fois definis on peut generer les targets en faissant un click sur la carte a l'interieur de la region defini prealablement. Lien vers [admin](https://192.168.75.31/admin/) 

 - lien vers les sites [public](https://192.168.75.31/public) (le test de l'api users est ici)

 - lien vers [api](https://192.168.75.31/api)

 
Plusieurs _"ajouts"_ ont ete realisees dans l'api de resources pour le bon deroulement du jeu

**Choix de logistique sur le jeu:**
 - Un joueur qui gagne la partie passe au niveau suivant, si le niveau n'existe pas l'IA va generer un
 - Un joueur perds uniquement si son ttl est fini

**Choix d'implementation des zones de jeu:**
 - la classe [GameZone](app/server/classes/GameZone.js) a ete cree pour definir les caracteristiques de chaque 'niveau':
    * `id` : un identifiant unique plutot pour designer le niveau (entier)
    * `center` : point central du jeu (Array [latitude, longitude])
    * `radius` : distance en metres au tour du centre
    * `ttl` : TimeToLive en minutes de chaque joueur dans ce niveau
    * `targets` : liste de position des targets (Array d'arrays de la forme : [latitude, longitude])


# TP4

 - Utilisation de vue-cli pour profiter des _Single File Components_ 

 - Vuex implemente, lien vers [store](app/client-vue/src/store/store.js)

 - Routeur implemente, les routes ont etait definies dans le [main](app/client-vue/src/main.js)

# TP5

 - **Géolocalisation inverse (IA sur la creation des jeux):**
    Si un joueur arrive a un niveau qui n'est pas encore cree, la fonction createGameZone de [GameFactory](app/server/classes/GameFactory.js) est appele pour creer une nouvelle zone de jeu (niveau) de caracteristiques suivantes:
    1. un `ttl` aleatoire entre 3 et 10 minutes
    2. un `radius` aleatoire entre (maxMeters * ttl) et une minMeters
        * maxMeters: quantite metres maximal par minute 
        * minMeters: pour regler le radius minimal (50m)
    3. un `centre` qui est la moyenne entre tous les jeux deja crees. Ceci pour ne pas trop eloigner la zone de jeu des jeux precedentes.
    4. des `targets` avec des positions aleatoires mais inclus dans le cercle defini par le centre et le radius

 - watch position implemented (pour les utilisateurs qui on donne la permission d'utiliser la geolocalisation)

 - syncchronisation client / serveur chaque 10 seg

 - vibration du telephone en cas de defaite

 - visualisation du countdown

 - Dark mode activable avec un switch


# TP6

- lien vers le [manifest](app/client-vue/dist/manifest.json), (il est cree au moment du build)

- Notifications simples, une de test au moment de start et une autre quand la partie est termine (letemps est fini)

- lien vers le [service worker](app/client-vue/public/service-worker.js)