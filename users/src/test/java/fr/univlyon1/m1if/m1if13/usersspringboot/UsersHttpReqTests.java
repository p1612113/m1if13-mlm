package fr.univlyon1.m1if.m1if13.usersspringboot;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;


@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
class UsersHttpReqTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void getUsersReturns200() {
		ResponseEntity<String> response = this.restTemplate.getForEntity("/users", String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}
	

	@Test
	public void getUsersAdminReturns200() {
		ResponseEntity<String> response = this.restTemplate.getForEntity("/users/admin", String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
	}

	@Test
	public void postUsersReturns201() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/x-www-form-urlencoded");
		MultiValueMap<String,String> body = new LinkedMultiValueMap<>();
		body.add("login", "test1");
		body.add("password", "test1");
		HttpEntity<MultiValueMap<String,String>> request = new HttpEntity<>(body, headers);
		ResponseEntity<String> response = this.restTemplate.exchange("http://localhost:8080/users", HttpMethod.POST, request, String.class);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
	}

	@Test
	public void putUsersReturns200() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/x-www-form-urlencoded");
		MultiValueMap<String, String> postBody = new LinkedMultiValueMap<>();
		postBody.add("login", "test2");
		postBody.add("password", "test2");
		HttpEntity<MultiValueMap<String, String>> postRequest = new HttpEntity<>(postBody, headers);
		ResponseEntity<String> postResponse = this.restTemplate.exchange("http://localhost:8080/users", HttpMethod.POST,
				postRequest, String.class);
		assertEquals(HttpStatus.CREATED, postResponse.getStatusCode());
		
		MultiValueMap<String, String> putBody = new LinkedMultiValueMap<>();
		putBody.add("password", "tset");
		HttpEntity<MultiValueMap<String, String>> putRequest = new HttpEntity<>(putBody, headers);
		ResponseEntity<String> putResponse = this.restTemplate.exchange("http://localhost:8080/users/test2", HttpMethod.PUT,
				putRequest, String.class);
		assertEquals(HttpStatus.OK, putResponse.getStatusCode());
	}

	@Test
	public void deleteUserReturns204() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Type", "application/x-www-form-urlencoded");
		MultiValueMap<String, String> postBody = new LinkedMultiValueMap<>();
		postBody.add("login", "test3");
		postBody.add("password", "test3");
		HttpEntity<MultiValueMap<String, String>> postRequest = new HttpEntity<>(postBody, headers);
		ResponseEntity<String> postResponse = this.restTemplate.exchange("http://localhost:8080/users", HttpMethod.POST,
				postRequest, String.class);
		assertEquals(HttpStatus.CREATED, postResponse.getStatusCode());

		MultiValueMap<String, String> deleteBody = new LinkedMultiValueMap<>();
		HttpEntity<MultiValueMap<String, String>> putRequest = new HttpEntity<>(deleteBody, headers);
		ResponseEntity<String> deleteResponse = this.restTemplate.exchange("http://localhost:8080/users/test3",
				HttpMethod.DELETE, putRequest, String.class);
		assertEquals(HttpStatus.NO_CONTENT, deleteResponse.getStatusCode());
	}


}
