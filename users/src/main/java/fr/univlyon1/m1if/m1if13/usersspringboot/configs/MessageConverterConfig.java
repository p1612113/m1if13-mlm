package fr.univlyon1.m1if.m1if13.usersspringboot.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class MessageConverterConfig implements WebMvcConfigurer{
    
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
         configurer.favorPathExtension(true).
                    favorParameter(false).
                    ignoreAcceptHeader(false).
                    defaultContentType(MediaType.APPLICATION_JSON).
                    mediaType("xml", MediaType.APPLICATION_XML). 
                    mediaType("json", MediaType.APPLICATION_JSON);  
    }
    
    // public HttpMessageConverter<User> createXmlHttpMessageConverter() {
    //     MarshallingHttpMessageConverter xmlConverter = new MarshallingHttpMessageConverter();
    

    //     final List<MediaType> mediaTypes = new ArrayList<>();
    //     mediaTypes.add(MediaType.APPLICATION_XML);
    //     mediaTypes.add(MediaType.APPLICATION_FORM_URLENCODED);

    //     final MarshallingHttpMessageConverter xmlConverter = new MarshallingHttpMessageConverter();
    //     xmlConverter.setSupportedMediaTypes(mediaTypes);

    //     xmlConverter.setMarshaller(xstreamMarshaller);
    //     xmlConverter.setUnmarshaller(xstreamMarshaller);
    //     return xmlConverter;
    // }
   
}