package fr.univlyon1.m1if.m1if13.usersspringboot.controller;

import javax.naming.AuthenticationException;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.bind.annotation.RestController;

import fr.univlyon1.m1if.m1if13.usersspringboot.configs.DaoConfig;
import fr.univlyon1.m1if.m1if13.usersspringboot.dao.UserDao;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.responses.ApiResponse;


@RestController
@CrossOrigin(origins = {"http://localhost:8081", "http://localhost", "http://192.168.75.31", "https://192.168.75.31"})
public class OperationController {

    // TODO récupérer le DAO...
    private ApplicationContext context = new AnnotationConfigApplicationContext(DaoConfig.class);
    @Autowired
    UserDao users = context.getBean("users", UserDao.class);

    
    /**
     * Procédure de login "simple" d'un utilisateur
     * 
     * @param login    Le login de l'utilisateur. L'utilisateur doit avoir été créé
     *                 préalablement et son login doit être présent dans le DAO.
     * @param password Le password à vérifier.
     * @return Une ResponseEntity avec le JWT dans le header "Authentication" si le
     *         login s'est bien passé, et le code de statut approprié (204, 401 ou
     *         404).
     */
    @PostMapping("/login")
    @Operation(
        summary = "Login operation",
        description = "The user must already have an account to login",
        operationId = "login",
        parameters = {
            @Parameter( name = "login", 
                description = "User's login identifier",
                required = true, allowEmptyValue = false),
            @Parameter( name = "password",
                description = "User's password",
                required = true, allowEmptyValue = false),
            @Parameter( name = "Origin", in = ParameterIn.HEADER,
                description = "Fetch request's origin",
                required = true, allowEmptyValue = false),
        },
        responses = {
            @ApiResponse(responseCode = "204", description = "Authentication succeeded"),
            @ApiResponse(responseCode = "401", description = "Unauthorized user"),
            @ApiResponse(responseCode = "404", description = "User not found"),
    })
    public ResponseEntity<Void> login(
        @RequestParam("login") String login,
        @RequestParam("password") String password,
        @RequestHeader("Origin") String origin) {
        
        HttpHeaders headers = new HttpHeaders();

        if (users.get(login).isPresent()) {
            try {
                users.get(login).get().authenticate(password);
                
                String token  =  JWT.create()
                                    .withSubject(origin)
                                    .sign(Algorithm.HMAC256("secret"));

                headers.add("Authentication", token);
                headers.add("Access-Control-Expose-Headers","Authentication");
                    
                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);

            } catch (AuthenticationException e) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unauthorized user");
            }

        }

        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
    }

    /**
     * Réalise la déconnexion
     */
    @DeleteMapping("/logout")
    @Operation(
        summary = "Logout operation",
        description = "The user's connection will be over",
        operationId = "logout",
        parameters =
            @Parameter( name = "login", 
                description = "User's login identifier",
                required = true, allowEmptyValue = false),
        responses = {
            @ApiResponse(responseCode = "204", description = "Authentication succeded"),
            @ApiResponse(responseCode = "404", description = "User not found")
    })
    public ResponseEntity<Void> logout(
        @RequestParam("login") String login) {
        
        if (users.get(login).isPresent()) {
            users.get(login).get().disconnect();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }

        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
    }
    // TODO

    /**
     * Méthode destinée au serveur Node pour valider l'authentification d'un utilisateur.
     * @param token Le token JWT qui se trouve dans le header "Authentication" de la requête
     * @param origin L'origine de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     */
    @GetMapping(value="/authenticate")
    @Operation(
        summary = "Authentification ",
        description = "Validates the authentification of a user",
        operationId = "authenticate",
        parameters = {
            @Parameter( name = "token", 
                description = "Token from the header 'Authentication'",
                required = true, allowEmptyValue = false),
            @Parameter( name = "origin", 
                description = "Origin of the request",
                required = true, allowEmptyValue = false)    
        },
        responses = {
            @ApiResponse(responseCode = "204", description = "Authentication succeded"),
            @ApiResponse(responseCode = "401", description = "Unauthorized user"),
            @ApiResponse(responseCode = "400", description = "Token verification failed"),
    })
    public ResponseEntity<Void> authenticate(
        @RequestParam("token") String token, 
        @RequestParam("origin") String origin) {
        // TODO

        try {
            JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256("secret")).build();
            String clientOrigin = jwtVerifier.verify(token).getSubject();            
            if(origin.equals(clientOrigin))
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            else
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Unauthorized user");

        } catch (JWTVerificationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Token verification failed");
        }

    }
}