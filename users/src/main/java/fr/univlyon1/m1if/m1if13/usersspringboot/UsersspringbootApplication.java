package fr.univlyon1.m1if.m1if13.usersspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class UsersspringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsersspringbootApplication.class, args);
		
	}

}
