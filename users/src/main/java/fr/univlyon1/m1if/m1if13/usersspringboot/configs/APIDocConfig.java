package fr.univlyon1.m1if.m1if13.usersspringboot.configs;

import fr.univlyon1.m1if.m1if13.usersspringboot.dao.UserDao;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springdoc.core.GroupedOpenApi;

@Configuration
public class APIDocConfig {

    @Bean(name="users-api")
    public GroupedOpenApi userOpenApi() {
        String packagesToscan[] = {"fr.univlyon1.m1if.m1if13.usersspringboot.controller"};
        return GroupedOpenApi.builder()
                .setGroup("users-api")
                .packagesToScan(packagesToscan)
                .build();
    }
    
}
