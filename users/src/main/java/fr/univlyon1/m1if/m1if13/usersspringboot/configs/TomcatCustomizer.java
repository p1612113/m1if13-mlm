package fr.univlyon1.m1if.m1if13.usersspringboot.configs;

import org.apache.catalina.connector.Connector;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.stereotype.Component;

@Component
public class TomcatCustomizer implements
  WebServerFactoryCustomizer<TomcatServletWebServerFactory> {
 
    @Override
    public void customize(TomcatServletWebServerFactory factory) {
        TomcatConnectorCustomizer tomcatConnectorCustomizers = new TomcatConnectorCustomizer(){
        
            @Override
            public void customize(Connector connector) {
                connector.setParseBodyMethods("POST,PUT,DELETE");
            }
        }; 
		factory.addConnectorCustomizers(tomcatConnectorCustomizers);
    }
}