package fr.univlyon1.m1if.m1if13.usersspringboot.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import fr.univlyon1.m1if.m1if13.usersspringboot.configs.DaoConfig;
import fr.univlyon1.m1if.m1if13.usersspringboot.dao.UserDao;
import fr.univlyon1.m1if.m1if13.usersspringboot.model.User;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
// import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
@OpenAPIDefinition(
    info = @Info(
        title = "M1IF13-2020 User's API",
        version = "0.0",
        description = "Description of the API of a web application using Spring Boot"
    ),
    servers = {
        @Server(description = "Local server", url = "http://localhost:8080"),
        @Server(description = "VM server", url = "http://192.168.75.31" ),
        @Server(description = "VM server (secure)", url = "https://192.168.75.31")
    }
)
public class RestUserController {
    
    ApplicationContext context = new AnnotationConfigApplicationContext(DaoConfig.class);
    @Autowired
    UserDao users = context.getBean("users", UserDao.class);
    
    /**
     * Get list of users logins (Accepts : JSON, XML)
     * @return Set of strings containing the users logins.
     */
    @GetMapping(
        value = "/users", 
        produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    @Operation(
        summary = "Get users",
        description = "Displays user's list",
        operationId = "getUsers",
        responses = {
            @ApiResponse(
                content = {
                    @Content(mediaType = "application/json"), 
                    @Content(mediaType = "application/xml"),
                    @Content(mediaType = "text/html")},
                responseCode = "200",
                description = "Successful display of the user's list"),
    })
	public @ResponseBody Set<String> getUsers() {
        return users.getAll();
    }

    /**
     * Get list of users logins (Accepts : HTML)
     * @return Set of strings containing the users logins.
     */
    @GetMapping(
        value = "/users", 
        produces = {MediaType.TEXT_HTML_VALUE})
    @ResponseStatus(HttpStatus.OK)
	public @ResponseBody ModelAndView getUsersHTML() {
        ModelAndView model = new ModelAndView("users.html");
        model.addObject("users", users.getAll());
        return model;
    }
    
    /**
     * Get user with login {id} (Accepts : JSON, XML)
     * @return User with login = id.
     */
    @GetMapping(
        value = "/users/{login}", 
        produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin(origins = {"http://localhost", "http://192.168.75.31", "https://192.168.75.31"})
    @Operation(
        summary = "Get user",
        description = "Get user by login",
        operationId = "getUser",
        parameters =
            @Parameter( name = "login", in = ParameterIn.PATH,
                description = "User's login identifier",
                required = true, allowEmptyValue = false),
        responses = {
            @ApiResponse(
                content = {
                    @Content(mediaType = "application/json"),            
                    @Content(mediaType = "application/xml"),
                    @Content(mediaType = "text/html")},
                responseCode = "200", 
                description = "User succesfully displayed"),
            @ApiResponse(responseCode = "404", description = "User not found")}
    )
    public @ResponseBody User getUser(@PathVariable String login) {
        if(users.get(login).isPresent())
            return users.get(login).get();
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
    }

    /**
     * Get user with login {id} (Accepts : HTML)
     * @return User with login = id.
     */
    @GetMapping(
        value = "/users/{login}", 
        produces = {MediaType.TEXT_HTML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    @CrossOrigin(origins = {"http://localhost:8081", "http://localhost", "http://192.168.75.31", "https://192.168.75.31"})
    public @ResponseBody ModelAndView getUserHTML( @PathVariable String login ) {

        if(users.get(login).isPresent()){
            ModelAndView model = new ModelAndView("user.html");
            model.addObject("user", users.get(login).get());
            return model;
        }
        
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
    }
    

    /**
     * Creation of user 
     * @param login
     * @param password
    */
    @PostMapping(
        value = "/users",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(
        summary = "Add user",
        description = "Adds user with given parameters login and password",
        operationId = "addUser",
        parameters = {
            @Parameter( name = "login", 
                description = "User's login identifier",
                required = true, allowEmptyValue = false),
            @Parameter( name = "password",
                description = "User's password",
                required = true, allowEmptyValue = false)
        },
        responses = {
            @ApiResponse(responseCode = "201", description = "User succesfully created"),
            @ApiResponse(responseCode = "400", description = "User already exist or some required parameters are not present")}
    )
    public void addUser(
        @RequestParam("login") String login,
        @RequestParam("password") String password){
        
        if( !users.get(login).isPresent() && (exists(login) && exists(password))){
            users.save(new User(login, password));
        }else
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exist or some required parameters are not present");
        
    }

    /**
     * Modification/Creation of login and password 
     * for the user with login id (Accepts : JSON , XML)
     * @param id 
     * @param login
     * @param password
     * @return user (login [string] and connected [boolean])
     */
    @PutMapping(
        value = "/users/{login}", 
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
        produces = {MediaType.APPLICATION_JSON_VALUE,MediaType.APPLICATION_XML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    @Operation(
        summary = "Update or create a user",
        description = "Updates user with given login and password or creates it in case it doesn't exists",
        operationId = "updateUser",
        parameters = {
            @Parameter( name = "login", in = ParameterIn.PATH,
                description = "User's login identifier",
                required = true, allowEmptyValue = false),
            @Parameter( name = "password",
                description = "User's password",
                required = true, allowEmptyValue = false)
        },
        responses = {
            @ApiResponse(
                content = {
                    @Content(mediaType = "application/json"),
                    @Content(mediaType = "application/xml"),
                    @Content(mediaType = "text/html")},
                responseCode = "200", 
                description = "User succesfully updated/created"),
            @ApiResponse(responseCode = "400", description = "Password parameter is missing")}
    )
    public @ResponseBody User updateUser(
        @PathVariable String login,
        @RequestBody String password) {

        updateORcreate(login, password);

        return users.get(login).get();
    }


    /**
     * Modification/Creation of login and password 
     * for the user with login id (Accepts : HTML)
     * @param id 
     * @param login
     * @param password
     * @return user (login [string] and connected [boolean])
     */
    @PutMapping(
        value = "/users/{login}",
        consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE},
        produces = {MediaType.TEXT_HTML_VALUE})
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody ModelAndView updateUserHTML(
        @PathVariable String login,
        @RequestBody String password) {
        
        updateORcreate(login, password);
        
        ModelAndView model = new ModelAndView("user.html");
        model.addObject("user", users.get(login).get());
        return model;
    }

    /**
     * Deletion of user
     * @param login
    */
    @DeleteMapping(value="/users/{login}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(
        summary = "Deletion of a user",
        description = "Deletes user from the user's list",
        operationId = "deleteUser",
        parameters =
            @Parameter( name = "login", in = ParameterIn.PATH,
                description = "User's login identifier",
                required = true, allowEmptyValue = false),
        responses = {
            @ApiResponse(responseCode = "204", description = "User succesfully deleted"),
            @ApiResponse(responseCode = "404", description = "User not found")}
    )
    public void deleteUser(
        @PathVariable String login) {
        
        if(users.get(login).isPresent()){
            users.delete(users.get(login).get());
        } else 
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
    }

    private boolean exists(String str){
        return !(str.isEmpty() || str == null);
    }

    private void updateORcreate(String login, String password){
        if(users.get(login).isPresent()){
            if(exists(password)){
                String[] params = new String[] {login, password};
                users.update(users.get(login).get(), params);
            } else
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Password parameter is missing");
        } else
            users.save(new User(login, password));

    }
}