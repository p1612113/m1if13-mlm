package fr.univlyon1.m1if.m1if13.usersspringboot.dao;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.naming.AuthenticationException;

import org.springframework.stereotype.Component;

import fr.univlyon1.m1if.m1if13.usersspringboot.model.User;

@Component
public class UserDao implements Dao<User> {

    private Set<User> users;

    @Override
    public Optional<User> get(String id) {
        // Search the user whose login is equals to id and return it
        for (User user : users) {
            if (user.getLogin().equals(id))
                return Optional.ofNullable(user);
        }
        // If such user is not found, return an empty Optional
        return Optional.empty();
    }

    @Override
    public Set<String> getAll() {
        return users.stream().map(user -> user.getLogin()).collect(Collectors.toSet());
    }

    @Override
    public void save(User user) {
        users.add(user);
    }

    @Override
    public void update(User user, String[] params) {
        users.remove(user);
        if (params.length > 0) {
            user.setLogin(params[0]);
            if (params.length > 1) {
                user.setPassword(params[1]);
            }
        }
        users.add(user);
    }

    @Override
    public void delete(User user) {
        users.remove(user);
    }

    public void authenticate(String login, String password) throws AuthenticationException {
        User user = get(login).get();
        user.authenticate(password);
        users.add(user);
    }

    public UserDao(Set<User> users) {
        this.users = users;
    }

    public UserDao(){
        users = new HashSet<>();
        users.add(new User("admin", "admin"));
   }

}