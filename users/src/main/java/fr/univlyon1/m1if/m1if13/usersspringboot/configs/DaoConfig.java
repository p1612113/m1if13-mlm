package fr.univlyon1.m1if.m1if13.usersspringboot.configs;

import fr.univlyon1.m1if.m1if13.usersspringboot.dao.UserDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = UserDao.class)
public class DaoConfig {

    @Bean(name = "users")
    @Autowired
    public UserDao getUsers() {
        return new UserDao();
    }
    
}