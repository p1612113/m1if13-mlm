const path = require("path");
const webpack = require("webpack");

module.exports = {
    mode: 'development',
    entry: "./public/src/js/index.js",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "dist/")
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
};