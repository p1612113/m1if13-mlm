import L from "leaflet"

export function reachedTarget(targets, position) {
    let userPos = L.latLng(position[0], position[1])
    let reached = targets.find(function(target){
        let targetPos = L.latLng(target.position[0], target.position[1])
        return userPos.distanceTo(targetPos) <= 2
    })
    return typeof reached !== 'undefined'
}