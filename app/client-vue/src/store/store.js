import Vue from 'vue'
import Vuex from 'vuex'
import { getGeoResources, getGameZone, putPosition, putUserWon } from '../repositories/resources'
import { reachedTarget } from '../logic/game'

Vue.use(Vuex)

function checkGeoLocalisation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
        function (pos) {
            localStorage.setItem("position", JSON.stringify([pos.coords.latitude, pos.coords.longitude]))
            localStorage.setItem("location_permission", true)
        }, 
        function (error) {
            switch(error.code) {
              case error.PERMISSION_DENIED:
                console.log("User denied the request for Geolocation.")
                break;
              case error.POSITION_UNAVAILABLE:
                console.log("Location information is unavailable.")
                break;
              case error.TIMEOUT:
                console.log("The request to get user location timed out.")
                break;
              case error.UNKNOWN_ERROR:
                console.log("An unknown error occurred.")
                break;
            }
            localStorage.setItem("location_permission", false)
        })
    }
}

export const store = new Vuex.Store({
    state: {
        user: {},
        players: [],
        targets: [],
        zoom: 15,
        gamezone: {},
        gameStarted: false,
        timeout: null,
        interval:null,
        message: "Welcome! ",
        notification : false,
        location : false
    },
    mutations: {
        setUser(state, user) { state.user = user },
        setZoom(state, zoom) { state.zoom = zoom; },
        setPlayers(state, players) { state.players = players; },
        setTargets(state, targets) { state.targets = targets; },
        setGameZone(state, gamezone) { state.gamezone = gamezone; },
        setGameStarted(state, start) { state.gameStarted = start },
        setTimeout(state, timeout) { state.timeout = timeout; },
        setInterval(state, interval) { state.interval = interval; },
        setMessage(state, message) { state.message = message; },
        setNotification(state, notification) { state.notification = notification; },
        setLocation(state, location) { state.location = location; }
    },
    actions: {
        Position({ commit, state }, {position, router}) {
            putPosition(position)
            .then((user) => {
                console.log("POSITION ", user)
                commit('setUser', user)
                let targets = state.targets
                if (reachedTarget(targets, position)) {
                    putUserWon(state.gamezone)
                    .then((values) => {
                        console.log("YOU WON VALUES",values)
                        commit('setUser', values.newUser)
                        commit('setGameZone', values.newGamezone)
                        commit('setPlayers', [])
                        commit('setTargets', [])
                        commit('setGameZone', {})
                        commit('setGameStarted', false)
                        commit('setMessage', "YOU WON!! try out the next level ;) ")
                        clearTimeout(state.timeout)
                        clearInterval(state.interval)
                        router.push({name:"won"})
                        /*
                        THINGS TO DO:
                            - stop the interval
                            - stop the game
                            - create a message.vue to show in case of :
                                - You Win!
                                - Time's over
                            - create a "alert" (simple <p> tag) to say " Someone else reached the target the game will be reloaded"
                        */
                        // commit('setGameStarted', false)
                        // clearInterval(interval)
                    })
                }
            })
        },
        Resources({ commit, state }) {
            if(state.gameStarted){
                console.log("RESOURCES GAME STARTED")

                commit('setInterval', setInterval(() => {
                    let level = state.user.level
                    let login = sessionStorage.getItem('login')
                    getGeoResources().then ((values) => {
                        let players = values.filter(res => res.role == 'player' && res.level == level && res.id != login)
                        commit('setPlayers', players)
                    })
                },10000))
            }
        },
        GameZone({ commit,  state }) {
            let level = Object.keys(state.user).length === 0 ? 0 : state.user.level
            getGameZone(level);
        },
        Timeout({commit, dispatch, state}, router){
            commit('setTimeout',setTimeout(function () {
                commit('setPlayers', [])
                commit('setTargets', [])
                commit('setGameZone', {})
                commit('setGameStarted', false)
                commit('setMessage', "TIME'S UP!!! :( wanna try again?")
                clearTimeout(state.timeout)
                clearInterval(state.interval)
                router.push({name:"lost"})
                if (state.notification){
                    var notifTitle = "TIME'S UP!! "
                    var notifBody = "try again...";
                    var options = {
                    body: notifBody
                    };
                    var notif = new Notification(notifTitle, options);
                    setTimeout(randomNotification, 5000);
                }
                window.navigator.vibrate(200);
            }, state.user.ttl * 60000))
        },
        Start({ commit }) {
            // checkGeoLocalisation()
            let position = [45.782, 4.8656]
            
            if (localStorage.getItem('position')){
                position = JSON.parse(localStorage.getItem("position"))
            }

            // commit('setLocation', JSON.parse(localStorage.getItem("location_permission")))

            // let position = localStorage.getItem("position")

            putPosition(position)
            .then((user) => {
                var level = user.level
                commit('setUser', user)

                getGameZone(level)
                .then((gamezone) => {
                    commit('setGameZone',gamezone)

                    getGeoResources()
                    .then((resources) => {
                        let login = sessionStorage.getItem('login'),
                            players = resources.filter(res => res.role == 'player' && res.level == level && res.id != login),
                            targets = resources.filter(res => res.role == 'target' && res.level == level)
                        
                        commit('setPlayers', players)
                        commit('setTargets', targets)
                        commit('setGameStarted', true)
                    })
                })
            })
        }

    },
    getters: {
        user: state => state.user,
        zoom: state => state.zoom,
        players: state => state.players,
        targets: state => state.targets,
        gamezone: state => state.gamezone,
        gameStarted: state => state.gameStarted,
        timeout: state => state.timeout,
        message: state => state.message,
        notification: state => state.notification,
        location: state => state.location
    }
})