import Vue from 'vue'
import VueRouter from 'vue-router'
import { store } from './store/store'
import vuetify from '@/plugins/vuetify'
import App from './App.vue'
import Container from './components/Container.vue';
import Login from './components/Login.vue';
import Message from './components/Message.vue';
import './registerServiceWorker'

Vue.config.productionTip = false
Vue.use(VueRouter);

const router = new VueRouter({
    routes : [
      { 
        path : '/',
        name : 'login',
        component : Login,        
        // component : Container,        
      },
      { 
        path: '/app',
        name : 'app',
        component : Container,
      },
      {
        path: '/app/won',
        name: 'won',
        component : Message
      },
      {
        path: '/app/lost',
        name: 'lost',
        component : Message
      }
    ],
    mode : 'history'
});

function authenticated() {
  return sessionStorage.getItem('token') !== null ;
}

router.beforeEach((to, from, next) => {
  if(!authenticated() && to.name !== 'login')
    next({name : 'login'});
  else
    next();
});

new Vue({
  store,
  router,
  vuetify,
  render: function (h) { return h(App) },
}).$mount('#app')
