import firebase from 'firebase/app'
import 'firebase/firestore';
import 'firebase/messaging';

const config = {
    apiKey: "AIzaSyAg2QIfVi8tQoLjoZoUDDRrlst3XcQ9624",
    authDomain: "m1if13-client-vue.firebaseapp.com",
    databaseURL: "https://m1if13-client-vue.firebaseio.com",
    projectId: "m1if13-client-vue",
    storageBucket: "m1if13-client-vue.appspot.com",
    messagingSenderId: "747791265614",
    appId: "1:747791265614:web:71689d3ffcbbf0d183381f",
    measurementId: "G-NPJ95PQ6V4"
}

firebase.initializeApp(config)

// Initialize Cloud Firestore through Firebase
let db = firebase.firestore();


// db.enablePersistence({experimentalTabSynchronization:true})

// const storage = firebase.storage()

const messaging = firebase.messaging()

export default {
  db,
  // storage,
  messaging
}