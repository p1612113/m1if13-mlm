import axios from "axios"

const base = 'https://192.168.75.31/api'

function unauthorized(){
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('login')
    console.warn("Unauthorized")
    location.reload();
}

export function getGeoResources() {
    return axios({
        method: 'get',
        url: base + '/resources',
        crossDomain: true,
        headers: {
            Authorization: sessionStorage.getItem('token'),
        }
    })
    .then(response => response.data )
    .catch(function (error) {
        console.log(error)
        unauthorized()
    });
}

export function getGameZone(level) {
    return axios({
        method: 'get',
        url: base + '/resources/games/' + level,
        crossDomain: true,
        headers: {
            Authorization: sessionStorage.getItem('token'),
        }
    })
    .then(response => {
        console.log("GET GAME ", response)
        return response.data})
    .catch(function (error) {
        console.log(error)
        unauthorized()
    });
}

export function putPosition(pos) {
    let resourceId = sessionStorage.getItem("login")
    return axios({
        method: 'put',
        url: base + '/resources/' + resourceId + '/position',
        data: { position: pos},
        crossDomain: true,
        headers: {
            Authorization: sessionStorage.getItem('token'),
        }
    })
    .then(response => response.data)
    .catch(function (error) {
        console.warn(error)
        unauthorized()
    });

}

export function putUserWon(gamezone){
    let resourceId = sessionStorage.getItem("login")
    return axios({
        method: 'put',
        url: base + '/resources/games/' + gamezone.id + '/players/'+ resourceId +'/won',
        crossDomain: true,
        headers: {
            Authorization: sessionStorage.getItem('token'),
        }
    })
    .then(response => response.status === 200 ? response.data : {})
    .catch(function (error) {
        console.warn(error)
        unauthorized()
    });

}