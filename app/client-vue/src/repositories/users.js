import axios from "axios"

const base = 'https://192.168.75.31/users'

function unauthorized(){
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('login')
    console.warn("Unauthorized")
    // location.reload();
}

export function postLogin(login, password) {
    let bodyFormData = new FormData()
    bodyFormData.set('login', login)
    bodyFormData.set('password', password)

    return axios({
        method: 'post',
        url: base + '/login',
        crossDomain : true,
        data: bodyFormData,
    })
    .then(response => response.headers.authentication)
    .catch(error => {
        console.log(error)
        unauthorized()
    });
}

export function deleteLogout() {
    let bodyFormData = new FormData();
    bodyFormData.set("login", sessionStorage.getItem("login"));
    return axios({
      method: "delete",
      url: base + '/logout',
      crossDomain: true,
      data: bodyFormData
    })
    .catch(function(error) {
        console.warn(error);
    });

}