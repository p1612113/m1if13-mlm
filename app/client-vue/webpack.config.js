
const path = require("path");
const webpack = require("webpack");
//const htmlPlugin = require('html-webpack-plugin');
const HtmlWebpackPlugin = require('vue-html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const dist = 'dist'
const WorkboxPlugin = require('workbox-webpack-plugin')

const { VueLoaderPlugin } = require('vue-loader');

module.exports = {
    mode : 'development',
    devtool : 'source-map',
    watch : true,
    entry : './src/main.js',
    //output : {
    //    path : path.resolve("./src/dist"),
    //    filename : 'bundle.js'
    //},
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            '@': path.resolve('src'),
        },
        extensions: ['*', '.js', '.vue', '.json']
    },
    module: {   
        rules: [
            {
                test: /\.css$/,
                use: [
                'style-loader',
                'css-loader',
                {
                    loader: 'postcss-loader',
                    options: {
                      plugins: (loader) => [
                        // Add the plugin
                        new IconfontWebpackPlugin(loader)
                      ]
                    }
                  }
                ],
            },{
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                'file-loader',
                ],
            },{
                test: /\.vue$/,
                use: [
                'vue-loader'
                ],
            },{
                test: /\.html$/,
                use : [
                'html-loader'
                ]
                
              },{
                test: /\.(eot|svg|ttf|woff|woff2)(\??\#?v=[.0-9]+)?$/,
                loader: 'file-loader?name=/fonts/[name].[ext]',
            }
        ],
    },
    
         
    plugins: [
        new CleanWebpackPlugin([dist]),
        new HtmlWebpackPlugin({
            filename: 'index.html',
            title: 'Get Started With Workbox For Webpack'
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        new VueLoaderPlugin(),
        new WorkboxPlugin.GenerateSW({
            // swDest: 'sw.js',
            swDest: 'service-worker.js',
            clientsClaim: true,
            skipWaiting: true,
        })
    ]
};