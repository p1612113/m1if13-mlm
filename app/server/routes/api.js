/* Router for /api */
var express = require('express')
var cors = require('cors')
const axios = require('axios').default
var router = express.Router()

router.use(express.json())
router.use(cors())

const GeoResource = require('../classes/GeoResources')
const GameZone = require('../classes/GameZone')
var gameFactory = require("../classes/GameFactory")
var targetFactory = require("../classes/TargetFactory")
//user resources
var resources = []

// games
var games = []

// middleware that is specific to this router
router.use(function (req, res, next) {
    next()
})
// define the admin page route
router.get('/resources', function (req, res) {

    authenticate(req).then(function (response) {
            console.log("Getting resources ")
            res.status(200).json(resources)
        
    }).catch(function (error) {
        console.log(error.message)
        // res.sendStatus(401)
    })
})

router.put('/resources/:resourceId/position', function (req, res) {
    authenticate(req).then(function (response) {

        let resourceId = req.params.resourceId
        let position = req.body.position
        
        if (!(Array.isArray(position) && position.length == 2)){
            res.status(400).send("Invalid position object "+position)
        }
        
        let index = 0
        let resource = resources.find((element, i) => {
            index = i
            return element.id == resourceId
        })

        if (!resource) {
            let gamezone = {}
            if (games.length > 0) 
                gamezone = games[0]
            else {
                gamezone = gameFactory.createGameZone(games)
                games.push(gamezone)
                resources = resources.concat(targetFactory.createTargets(gamezone))
            }
            resource = new GeoResource(resourceId, position, gamezone.ttl)
            resources.push(resource)
            index = resources.length - 1
        } else resource.position = position

        resources[index] = resource
        console.log("Update position ", resource.position)
        res.status(200).json(resource)

    }).catch(function (error) {
        console.log(error.message)
        // res.sendStatus(401)
    })
})

router.put('/resources/:resourceId/image', function (req, res) {
    authenticate(req).then(function (response) {
        let resourceId = req.params.resourceId
        let url = req.body.url

        if (typeof url != "string") {
            res.status(400).send("Invalid image URL object")
        }

        let resource = resources.find(element => element.id == resourceId)
        if (!resource) {
            res.sendStatus(404)
        }

        resource.url = url

        res.sendStatus(204)

    }).catch(function (error) {
        console.log(error.message)
        // res.sendStatus(401)
    })
})

router.get('/resources/games/:gameId', function (req, res) {
    authenticate(req).then(function (response) {
        let gameId = req.params.gameId

        // console.log("Getting gamezone ", gameId, " games ", games, " games[gameId] ", games[gameId])
        
        if(typeof games[gameId] == undefined){
            console.log("game not found ", games[gameId])
            games[gameId] = gameFactory.createGameZone(games)
            resources = resources.concat(targetFactory.createTargets(games[gameId]))
        }
        console.log("GAMEZONE ", games[gameId])
        res.status(200).json(games[gameId])

    }).catch(function (error) {
        console.log(error.message)
        // res.sendStatus(401)
    })
})

router.post('/resources/games', function (req, res) {
    let center = req.body.center,
        radius = req.body.radius,
        ttl = req.body.ttl,
        targets = req.body.targets

    if (!(Array.isArray(center)
        && center.length == 2
        && typeof center[0] == 'number'
        && typeof center[1] == 'number')) {

        res.status(400).send("Invalid center object")
    }

    if (typeof radius != 'number')
        radius = parseInt(radius)
        // res.status(400).send("Invalid radius number")

    if (!(Array.isArray(targets))) {
        res.status(400).send("Invalid target object")
    }

    if (typeof ttl != 'number')
        ttl = parseInt(ttl)
        // res.status(400).send("Invalid TTL number")

    let gamezone = new GameZone(games.length, radius, center, ttl, targets)
    
    games.push(gamezone)
    resources = resources.concat(targetFactory.createTargets(gamezone))

    res.status(200).json(gamezone)
})

router.put('/resources/games/:gameId/players/:playerId/won', function (req, res) {
    authenticate(req).then(function (response) {
        let gameID = req.params.gameId
        let playerId = req.params.playerId
        let index = 0
        let game = games.find((g,i) => { index = i ; return g.id == gameID })
        let user = {}, userIndex = 0
        let ids = game.targets.map((t, i) => gameID + "" + i)
        game.targets = gameFactory.changeTargets(game)

        resources.forEach((res, i) => {
            if (res.id === playerId) {
                res.level++
                res.trophys.push(res.level)
                userIndex = i
                user = res
            }
            if (ids.includes(res.id)) {
                res.position = game.targets.find((t,i) => res.id == gameID+""+i)
            }
        })
        games[index] = game

        let gamezone = {}
        if (typeof games[user.level] != 'undefined') 
            gamezone = games[user.level]
        else {
            gamezone = gameFactory.createGameZone(games)
            games.push(gamezone)
            resources = resources.concat(targetFactory.createTargets(gamezone))
        }

        resources[userIndex].ttl = gamezone.ttl
        response = {
            newUser : user,
            newGamezone : gamezone
        }
        res.status(200).json(response)
    }).catch(function (error) {
        console.log(error.message)
        // res.sendStatus(401)
    })
})


module.exports = router

function authenticate(req) {
    let authToken = req.get("Authorization")
    let origin = req.get("Origin")

    if (!origin)
        origin = "https://192.168.75.31"

    return axios({
        method: 'get',
        url: 'http://192.168.75.31:8080/authenticate',
        params: {
            token: authToken,
            origin: origin
        }
    });
}