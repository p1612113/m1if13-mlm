/* Router for /admin */

var express = require('express')
const axios = require('axios').default
var bodyParser = require('body-parser')
const qs = require('querystring')


var router = express.Router()
var urlencodedParser = bodyParser.urlencoded({ extended: false }) // to make form data available


// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    console.log('Time: ', Date.now())
    next()
})
// define the admin page route
router.get('/', function (req, res) {
    axios({
        method: 'get',
        url: 'http://192.168.75.31:8080/users',
        headers: { 'Accept': 'application/json' },
        timeout: 5000
    }).then(function (response) {

        res.render('admin', { users: response.data })

    })
})

// USER page
router.get('/users/:login', function (req, res) {
    axios({
        method: 'get',
        url: 'http://192.168.75.31:8080/users/' + req.params.login,
        headers: { 'Accept': 'application/json' },
        timeout: 5000
    }).then(function (response) {
        console.log
        console.log(response.data)
        res.render('user', { user: response.data })
    }).catch(function (error) {
        res.redirect('/admin')
    })
})

router.post('/users', urlencodedParser, function (req, res) {
    axios({
        method: 'post',
        url: 'http://192.168.75.31:8080/users',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: qs.stringify(req.body),
        timeout: 5000
    }).then(function (response) {

        res.redirect('/admin')

    }).catch(function (error) {
        console.log(error)
        res.redirect('/admin')
    })
})

router.put('/users/:login', urlencodedParser, function (req, res) {
    console.log("PUT in users")
    console.log(req.body)
    axios({
        method: 'put',
        url: 'http://192.168.75.31:8080/users/' + req.params.login,
        headers: { 'Accept': 'application/json' },
        timeout: 5000
    }).then(function (response) {

        res.redirect('/admin')

    }).catch(function (error) {
        console.log(error)
        res.redirect('/admin')
    })
})

router.delete('/users/:login', function (req, res) {
    console.log("DELETE in users")
    axios({
        method: 'delete',
        url: 'http://192.168.75.31:8080/users/' + req.params.login,
        headers: { 'Accept': 'application/json' },
        timeout: 5000
    }).then(function (response) {

        res.redirect('/admin')

    }).catch(function (error) {
        console.log(error)
        res.redirect('/admin')
    })
})


module.exports = router