class GameZone {

    constructor(
        id = 0,
        radius = 750,
        center = [45.782,4.8656],     // LatLng of Leaflet     
        ttl = 10,
        targets = [[45.782,4.8656]]
    ) {

        this.id = id
        this.radius = radius
        this.center = center
        this.ttl = ttl
        this.targets = targets
    }
}

module.exports = GameZone;