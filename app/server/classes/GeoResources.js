class GeoRessource {
    // id = "toto";
    // url = "http://example.com/users/toto/avatar.png";
    // position = [0];
    // blurred = true;
    // role = "infected";
    // ttl = 0;
    // status = "alive";
    // trophys = [{ action: "infected" }];

    constructor(
        id = "admin",
        position = [45.782,4.8656],     // LatLng of Leaflet     
        ttl = 10,
        role = "player", // player or target
        level = 0,
        status = "playing", //playing | eliminated | element
        url = "https://images.pexels.com/photos/556667/pexels-photo-556667.jpeg",
        trophys = []
    ) {


        this.id = id;
        this.url = url;
        this.position = position;
        this.role = role;
        this.level = level;
        this.ttl = ttl;
        this.status = status;
        this.trophys = trophys;
    }
}


module.exports = GeoRessource;