const GameZone = require('./GameZone')
var defaultCenter = [45.782,4.8656] // Natibus

function getCenter (games) {
    if(games.length > 0){
        let X = 0, Y = 0
        games.forEach(game => {
            X += game.center[0]
            Y += game.center[1]
        });
        let latitude = X/games.length + (Math.random() * 0.0001)
        let longitude = Y/games.length + (Math.random() * 0.0001)
        
        return [latitude, longitude]
    }
    return defaultCenter 
}

function getDistance(lat1,lon1,lat2,lon2) {
    var R = 6371 // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1)  // deg2rad below
    var dLon = deg2rad(lon2-lon1) 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)

    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)) 
    var d = R * c // Distance in km
    return d/1000
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}

function getTargets (center, radius, numberTargets = 3 ) {
    let targets = []
    for (let i = 0; i < numberTargets; i++) {
        let x, y, r = radius/111111
        do {
            x = Math.random() ; y = Math.random()
            x = x > 0.5 ? (x * r) + center[0] : (-x * r) + center[0]
            y = y > 0.5 ? (y * r) + center[1] : (-y * r) + center[1]
        } while(getDistance(center[0], center[1], x, y) > radius)

        targets.push([x,y])
    }
    return targets
}

module.exports = {    

    changeTargets : function (game) {
        return getTargets(game.center, game.radius, game.targets.length)
    },

    createGameZone : function (games) {
        var minMeters = 50, // for the radius
            maxMeters = 100 // maximum meters that can be walked in a minute
        let ttl = Math.floor(Math.random() * 10) + 3, 
            radius = Math.floor(Math.random() * maxMeters * ttl) + minMeters,
            center = getCenter(games),
            targets = getTargets(center, radius) // 3 targets 
    
        return new GameZone(games.length, radius, center, ttl, targets) 
    }
}
