const GeoResource = require('./GeoResources')

module.exports = {

    createTargets : function (game) {
        let targetRes = game.targets.map(function (position, index) {
            let id = "target" + game.id + index
            return new GeoResource(id, position, game.ttl, "target", game.id, "element")
        })
        return targetRes
    }
}
