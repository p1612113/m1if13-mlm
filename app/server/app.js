const express = require('express')
var methodOverride = require('method-override')

const app = express()

app.set('views', './views')
app.set('view engine', 'ejs')

const port = 3376 // express.Router for /admin
var admin = require('./routes/admin')
var api = require('./routes/api')



app.get('/', (req, res) => res.send('Hello World!'))
app.use('/static', express.static('public'))

app.use('/api', api)

app.use(methodOverride('_method')) // Used to make PUT & DELETE reqs in forms
app.use('/admin', admin)


// 404 response. Must be last.
app.use(function (req, res, next) {
    res.status(404).send("Sorry can't find that!")
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))